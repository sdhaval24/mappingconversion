package com.company;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Main {

    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) throws URISyntaxException {
        JSONParser parser = new JSONParser();
        //reading files from the path
        URL url = Main.class.getClassLoader().getResource("xmlToJson");
        if (url == null) {
            System.out.println("Directory Not Found");
        } else {
            logger.info("Reading all the files");
            File filePath = Paths.get(url.toURI()).toFile();
            File[] listOfFiles = filePath.listFiles();
            //to store source-target
            Map<String, String> mapping = new HashMap<>();
            for (File file : listOfFiles) {
                String fileName = file.getName();
                logger.info("Reading --> "+ fileName);
                //removing extension
                fileName = fileName.substring(0, fileName.lastIndexOf('.'));
                try (FileReader reader = new FileReader(file.getAbsolutePath())) {
                    Object obj = parser.parse(reader);
                    if (!fileName.contains("Enums")) {
                        JSONObject xmlToJson = (JSONObject) obj;
                        //storing top node
                        JSONObject topNode = (JSONObject) xmlToJson.get("xmlToJson");
                        JSONArray result = (JSONArray) topNode.get(fileName);
                        for (int i = 0; i < result.size(); i++) {
                            JSONObject objects = (JSONObject) result.get(i);
                            String path = (String) objects.get("source");
                            JSONArray targets = (JSONArray) objects.get("targets");
                            if (targets != null) {
                                for (int j = 0; j < targets.size(); j++) {
                                    JSONObject childNode = (JSONObject) targets.get(j);
                                    String sapiNode = childNode.get("target").toString();
                                    sapiNode = sapiNode.replace("*", "{ref}");
                                    mapping.put(path + "/" + childNode.get("source").toString(), sapiNode);
                                }
                            } else {
                                mapping.put(objects.get("source").toString(), objects.get("target").toString());
                            }
                        }
                    } else {
                        JSONObject enumJson = (JSONObject) obj;
                        Iterator<String> enumKeys = enumJson.keySet().iterator();
                        while (enumKeys.hasNext()) {
                            String enumKey = enumKeys.next();
                            JSONObject node = (JSONObject) enumJson.get(enumKey);
                            enumKey = enumKey.replace("*", "{ref}");
                            Iterator<String> nodeKeys = node.keySet().iterator();
                            while (nodeKeys.hasNext()) {
                                String nodeKey = nodeKeys.next();
                                String nodeValue = (String) node.get(nodeKey);
                                mapping.put(enumKey + "/" + nodeKey, nodeValue.toString());
                            }
                        }
                    }

                    //storing into csv file
                    storeIntoCSV(mapping, fileName);
                    mapping.clear();
                } catch (FileNotFoundException e) {
                    logger.error("Failed File->" + fileName,e);
                    //System.out.println("Failed File->" + fileName);
                } catch (IOException e) {
                    logger.error("Failed File->" + fileName,e);
                } catch (ParseException e) {
                    logger.error("Failed File->" + fileName,e);
                } catch (Exception e) {
                    logger.error("Failed File->" + fileName,e);
                }
            }
        }
    }

    /**
     * It stores ACORD->SAPI KEY_VALUE PAIR into CSV file
     *
     * @param mapping : Hashmap of source:target value
     */
    private static void storeIntoCSV(Map<String, String> mapping, String fileName) throws URISyntaxException {
        File dir = new File(System.getProperty("user.dir") + "/ACRODtoSAPImappings");
        if (!dir.exists()) {
            dir.mkdir();
        }
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(dir.getAbsolutePath() + "/" + fileName + ".csv");
        } catch (FileNotFoundException e) {
            logger.error("Failed File->" + fileName,e);
        }
        StringBuilder sb = new StringBuilder();
        String columnNameList = "ACORD,SAPI";
        sb.append(columnNameList + "\n");
        for (Map.Entry<String, String> a : mapping.entrySet()) {
            sb.append(a.getKey() + "," + a.getValue() + "\n");
        }
        pw.write(sb.toString());
        pw.close();
        logger.info("Stored into CSV");
    }
}
