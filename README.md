### Mapping Validation Automation ###

#### Quick summary
* This is a simple java application that quickly generates CSV file from JSON file to provide ACORD -> SAPI Mappings
#### Summary of set up
* Clone the project and open in you favorite choice of IDE. 
#### How to run application
* mvn clean
* mvn install 
* copy files and paste inside xmlToJson Folder inside resources
* Run application 
* Corresponding CSV file be created inside ACORDtoSAPImappings Folder